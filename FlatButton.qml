﻿import QtQuick 2.2
import QtQuick.Controls 2.0

Rectangle {
    id: bkgnd;
    implicitWidth: 100;
    implicitHeight: 30;
    color: "transparent";
    property alias iconSource: icon.source;
    property alias iconWidth: icon.width;
    property alias iconHeight: icon.height;
    property alias textColor: btnText.color;
    property alias font: btnText.font;
    property alias text: btnText.text;
    property alias toolTipText: toolTip.text;
    //property alias hovere: bkgnd.hovered;
    radius: 4;
    property bool hovered: false;
    property bool tiphovered: false;
    border.color: "lightsteelblue";
    border.width: hovered ? 2 : 1;
    signal clicked;
    signal editingFinished;

    ToolTip {
        id: toolTip;
        visible: tiphovered;
        delay: 500;
        timeout: 2500;
    }
    Image {
        id: icon;
        anchors.left: parent.left;
        anchors.verticalCenter: parent.verticalCenter;
    }
    TextInput {
        id: btnText;
        width: bkgnd.implicitWidth - 10;
        anchors.left: icon.right;
        anchors.centerIn: parent;
        anchors.verticalCenter: icon.verticalCenter;
        anchors.margins: 4;
        font.pointSize: 8;
        readOnly: false;
        color: ma.pressed ? "blue" : (parent.hovered ? "#0000a0" : "deeppink");
        onActiveFocusChanged: {
            if (activeFocus) {
                selectAll()
            }
        }
        onEditingFinished: {
            bkgnd.editingFinished();
        }

        MouseArea {
            id: ma;
            anchors.fill: parent;
            hoverEnabled: true;
            onEntered: {
                bkgnd.tiphovered = true;
            }
            onExited: {
                bkgnd.tiphovered = false;
            }
            onClicked: {
                bkgnd.clicked();
                btnText.focus = true;
            }
        }
    }
}
