#include "beziercurve.h"
Q_DECLARE_METATYPE(QAbstractSeries *)
Q_DECLARE_METATYPE(QAbstractAxis *)
#include <QtCharts/QXYSeries>
#include <QtCharts/QAreaSeries>
#include <QtCore/QtMath>
#include <Qdebug>

BezierCurve::BezierCurve(QObject *parent) : QObject(parent)
{

}

void BezierCurve::createBezierCurve(QPointF start, QPointF end, QPointF p1, QPointF p2, QPointF p3, QPointF p4)
{
    QPointF a = start;
    QPointF b = p1;
    QPointF c = p2;
    QPointF d = p3;
    QPointF e = p4;
    QPointF f = end;
    m_curve.clear();
    for (double k = 0; k <= 100; k += m_step) {
        double t = k / 100.0;
        double x_val = (a.x() * pow((1 - t), 5)) + (5 * b.x() * t * pow((1 - t), 4)) + (10 * c.x() * pow(t, 2) * pow ((1 - t), 3))
             + (10 * d.x() * pow(t, 3) * pow((1 - t), 2)) + (5 * e.x() * pow(t, 4) * (1 - t)) + (f.x() * pow(t, 5));
        double y_val = (a.y() * pow((1 - t), 5)) + (5 * b.y() * t * pow((1 - t), 4)) + (10 * c.y() * pow(t, 2) * pow ((1 - t), 3))
             + (10 * d.y() * pow(t, 3) * pow((1 - t), 2)) + (5 * e.y() * pow(t, 4) * (1 - t)) + (f.y() * pow(t, 5));

        //printf("%d : x %f y %f -- %f\n", i++, x_val, y_val, t);//输出点
        m_curve.push_back(QPointF(x_val, y_val));
    }
}

/* angle => [-PI, PI) */
static double adjust_angle_rad(double angle)
{
    angle = fmod(angle, 2 * M_PI);

    if (angle < -M_PI)
        angle += 2 * M_PI;
    else if (angle > M_PI)
        angle -= 2 * M_PI;
    else if(angle == M_PI)
        angle = -M_PI;

    return angle;
}

/**
* 简单地生成3×3的坐标变换矩阵
* T =
* */
static void get_T_matrix(dof3_rad_t pos, double T[3][3]){
    double temp[3][3] = {
        { cosf(pos.a_rad), -sinf(pos.a_rad), pos.x },
        { sinf(pos.a_rad), cosf(pos.a_rad), pos.y },
        { 0, 0, 1 }
    };
    memcpy(T, temp, sizeof(temp));
    return;
}

static void Matrix_Mul(double *Mul1, int Mul1_m, double *Mul2, int Mul2_n, int nm, double *Mul)
{
    //Mul1[Mul1_m][nm]*Mul2[nm][Mul2_n]=Mulĳ˷
    int i, j, k, a, b, c, d;
    for (i = 0, a = 0, c = 0; i<Mul1_m; i++)
    {
        for (j = 0; j<Mul2_n; j++)
        {
            b = a + j;
            Mul[b] = 0;
            for (k = 0, d = 0; k<nm; k++)
            {
                Mul[b] += Mul1[c + k] * Mul2[d + j];
                d += Mul2_n;
            }
        }
        c += nm;
        a += Mul2_n;
    }
}

/**
* 从3×3坐标变换矩阵生成坐标
*
* */
void get_pos_from_T_matrix(double T[3][3], dof3_rad_t *p){
    if (p == NULL) return;
    p->x = T[0][2];
    p->y = T[1][2];
    p->a_rad = adjust_angle_rad(atan2(T[1][0], T[0][0]));
}

static dof3_rad_t pose_mul(const dof3_rad_t left, const dof3_rad_t right)
{
    dof3_rad_t mul;
    double mat_mul[3][3];
    double mat_left[3][3];
    double mat_right[3][3];
    get_T_matrix(left, mat_left);
    get_T_matrix(right, mat_right);
    Matrix_Mul((double*)mat_left, 3, (double*)mat_right, 3, 3, (double*)mat_mul);
    get_pos_from_T_matrix(mat_mul, &mul);
    return mul;
}

QVariantList BezierCurve::calcPoint(QPointF start, qreal s_rad, QPointF end, qreal e_rad)
{
    double params[3];

    double smooth_rate = 0;
    double turn_rate = 0;
    double disStartEnd = sqrtf((start.x() - end.x()) * (start.x() - end.x())
                              + (start.y() - end.y()) * (start.y() - end.y()));
    double Badj = 0;
    qDebug() << disStartEnd << " " << s_rad << " " << e_rad;
    if (fabsf(adjust_angle_rad(s_rad - e_rad)) > M_PI / 4) {
        Badj = disStartEnd / 1.414; //(*到交点距离，应严格算*)
        smooth_rate = 0.4;
        turn_rate = 0.7; //(*中间转弯点五次贝塞尔第三与第四点，越大越急*)
    }
    else {
        Badj = disStartEnd;
        smooth_rate = 0.2;
        turn_rate = 0.4; //(*中间转弯点五次贝塞尔第三与第四点，越大越急*)
    }
    //与道宽、平滑度相关，这个应该由服务器提供。
    params[0] = Badj;
    params[1] = smooth_rate;
    params[2] = turn_rate;

    qDebug() << "params[0] = " << params[0] << " params[1] = " << params[1] << " params[2] = " << params[2];
    QPointF pt1, pt2, pt3, pt4;
    pt1.rx() = params[0] * params[1];
    pt1.ry() = 0;

    pt2.rx() = params[0] * params[2];
    pt2.ry() = 0;

    pt3.rx() = -1 * params[0] * params[2];
    pt3.ry() = 0;

    pt4.rx() = -1 * params[0] * params[1];
    pt4.ry() = 0;
    qDebug() << "pt1 = " << pt1 << " pt2 = " << pt2 << " pt3 = " << pt3 << " pt4 = " << pt4;

    dof3_rad_t pt1w = pose_mul(dof3_rad_t(start.x(), start.y(), 0), dof3_rad_t(pt1.x(), pt1.y(), 0));
    pt1.rx() = pt1w.x;
    pt1.ry() = pt1w.y;

    dof3_rad_t pt2w = pose_mul(dof3_rad_t(start.x(), start.y(), 0), dof3_rad_t(pt2.x(), pt2.y(), 0));
    pt2.rx() = pt2w.x;
    pt2.ry() = pt2w.y;

    dof3_rad_t pt3w = pose_mul(dof3_rad_t(end.x(), end.y(), 0), dof3_rad_t(pt3.x(), pt3.y(), 0));
    pt3.rx() = pt3w.x;
    pt3.ry() = pt3w.y;

    dof3_rad_t pt4w = pose_mul(dof3_rad_t(end.x(), end.y(), 0), dof3_rad_t(pt4.x(), pt4.y(), 0));
    pt4.rx() = pt4w.x;
    pt4.ry() = pt4w.y;

    QVariantList pt;
    pt.push_back(QVariant(pt1));
    pt.push_back(QVariant(pt2));
    pt.push_back(QVariant(pt3));
    pt.push_back(QVariant(pt4));
    return pt;
}

void BezierCurve::update(QAbstractSeries *series)
{
    if (m_curve.count() == 0) {
        return;
    }
    if (series) {
        QXYSeries *xySeries = static_cast<QXYSeries *>(series);
        xySeries->replace(m_curve);
    }
}
