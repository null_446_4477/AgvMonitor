#ifndef BEZIERCURVE_H
#define BEZIERCURVE_H

#include <QObject>
#include <QtCharts/QAbstractSeries>
QT_CHARTS_USE_NAMESPACE

struct dof3_rad_t {
    dof3_rad_t(double _x, double _y, double _a)
    {
        x = _x;
        y = _y;
        a_rad = _a;
    }
    dof3_rad_t()
    {
        x = y = a_rad = 0;
    }
    float x;
    float y;
    float a_rad;
};

class BezierCurve : public QObject
{
    Q_OBJECT
public:
    explicit BezierCurve(QObject *parent = nullptr);
    Q_INVOKABLE void createBezierCurve(QPointF start, QPointF end, QPointF p1, QPointF p2, QPointF p3, QPointF p4);
    Q_INVOKABLE void update(QAbstractSeries *series);
    Q_INVOKABLE QVariantList calcPoint(QPointF start, qreal s_rad, QPointF end, qreal e_rad);
signals:

public slots:
private:
    QVector<QPointF> m_curve;   // 存储生成的贝塞尔曲线点
    double m_step = 0.1;              // 步进值
};

#endif // BEZIERCURVE_H
