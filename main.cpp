#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtWidgets/QApplication>
#include <QQmlContext>
#include "beziercurve.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qmlRegisterType<BezierCurve>("Qt.BezierCurve", 1, 0, "BezierCurve");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    BezierCurve bezierCurve;
    engine.rootContext()->setContextProperty("bezierCurve", (QObject *)&bezierCurve);

    return app.exec();
}
