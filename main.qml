import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtCharts 2.1

Window {
    id: root;
    visible: true;
    width: 840;
    height: 480;
    color: "#EEEEEE";
    title: qsTr("AGV Monitor")
    property var pointIndex: -1
    property var hoveredPoint: 0;
    property var maxAxisX: 50;
    property var minAxisX: -50;
    property var minAxisY: -50;
    property var maxAxisY: 50;

    // 贝塞尔曲线6个点存入数组
    property var myList:new Array();
    Row {
        ChartView {
            id: agvMap;
            width: root.width - 200;
            height: root.height;
            animationOptions: ChartView.NoAnimation;
            theme: ChartView.ChartThemeDark;
            Component.onCompleted: {
            }
            ValueAxis {
                id: axisY;
                min: minAxisY;
                max: maxAxisY;
                labelFormat: "%.1f m";
            }
            ValueAxis {
                id: axisX;
                min: minAxisX;
                max: maxAxisX;
                labelFormat: "%.1f m";
            }
            LineSeries {
                id: lineSeries1;
                name: "agv map";
                axisX: axisX;
                axisY: axisY;
                useOpenGL: true;
            }
           ScatterSeries {
               id: scatter;
               markerShape: ScatterSeries.MarkerShapeRectangle;
               XYPoint { x: 0; y: 0 }
           }
           ScatterSeries {
               id: startPointScat;
               visible: false;
               markerShape: ScatterSeries.MarkerShapeCircle;
               borderWidth: 0;
           }
           ScatterSeries {
               id: stopPointScat;
               visible: false;
               markerShape: ScatterSeries.MarkerShapeCircle;
               borderWidth: 0;
               markerSize: 10;
           }
           ScatterSeries {
               id: pointScat;
               visible: false;
               markerShape: ScatterSeries.MarkerShapeCircle;
               borderWidth: 0;
               markerSize: 10;
               XYPoint {id: onePoint;}
               XYPoint {id: twoPoint;}
               XYPoint {id: threePoint;}
               XYPoint {id: fourPoint;}
           }
           MouseArea {
               anchors.fill: parent
               hoverEnabled: true
               onPositionChanged: {
                   var point = Qt.point( 0, 0 )
                   point.x = mouse.x
                   point.y = mouse.y
                   hoveredPoint = parent.mapToValue(point, scatter);
                   xPositionValue.text = hoveredPoint.x.toFixed(10);
                   yPositionValue.text = hoveredPoint.y.toFixed(10);
               }
               onClicked: {
                   choosePoint();
                   allNoChoose();
               }
           }
        }
        Rectangle {
            width: root.width - agvMap.width;
            height: root.height;
            GroupBox {
                id: textGropBox;
                width: root.width - agvMap.width - 6;
                height: root.height;
                Column{
                    x: 2;
                    y: 2;
                    spacing: 10;
                    Row {
                        Text {
                            id: xPosition
                            text: qsTr("x: ")
                        }
                        Text {
                            id: xPositionValue
                            width: 20;
                            text: qsTr("0")
                        }
                    }
                    Row {
                      Text {
                          id: yPosition
                          text: qsTr("y: ")
                      }
                      Text {
                          id: yPositionValue
                          width: 20;
                          text: qsTr("0")
                      }
                    }
                    Row {
                        spacing: 5;
                        TextField {
                            id: zoomValue;
                            width: 30;
                        }
                        Button {
                            id: zoom;
                            text: "button";
                            onClicked: {
                                agvMap.zoom(zoomValue.text);
                            }
                        }
                    }
                    Column {
                        spacing: 5;
                        Row {
                            spacing: 5;
                            FlatButton {
                                id: start;
                                width: 100;
                                toolTipText: "start point";
                                onClicked: {
                                    pointIndex = 0;
                                    buttonFun(start);
                                }
                                onEditingFinished: {
                                    myList[0] = + (start.text);
                                    updatePoint();
                                    start.hovered = false;
                                }
                            }
                            TextField {
                                id: startRot;
                                text: "0";
                                style: textFieldComp2;
                            }
                        }
                        Row {
                            spacing: 5;
                            FlatButton {
                                id: stop;
                                width: 100;
                                toolTipText: "stop point";
                                onClicked: {
                                    pointIndex = 1;
                                    buttonFun(stop);
                                }
                                onEditingFinished: {
                                    myList[1] = textTopPoint(stop.text);
                                    updatePoint();
                                    stop.hovered = false;
                                }
                            }
                            TextField {
                                id: stopRot;
                                text: "0";
                                style: textFieldComp2;
                            }
                        }
                        FlatButton {
                            id: one;
                            width: 100;
                            toolTipText: "one point";
                            onClicked: {
                                pointIndex = 2;
                                buttonFun(one);
                            }
                            onEditingFinished: {
                                myList[2] = textTopPoint(one.text);
                                updatePoint();
                                one.hovered = false;
                            }
                        }
                        FlatButton {
                            id: two;
                            width: 100;
                            toolTipText: "two point";
                            onClicked: {
                                pointIndex = 3;
                                buttonFun(two);
                            }
                            onEditingFinished: {
                                myList[3] = textTopPoint(two.text);
                                updatePoint();
                                two.hovered = false;
                            }
                        }
                        FlatButton {
                            id: three;
                            width: 100;
                            toolTipText: "three point";
                            onClicked: {
                                pointIndex = 4;
                                buttonFun(three);
                            }
                            onEditingFinished: {
                                myList[4] = textTopPoint(three.text);
                                updatePoint();
                                three.hovered = false;
                            }
                        }
                        FlatButton {
                            id: four;
                           // width: 100;
                            toolTipText: "four point";
                            onClicked: {
                                pointIndex = 5;
                                buttonFun(four);
                            }
                            onEditingFinished: {
                                myList[5] = textTopPoint(four.text);
                                updatePoint();
                                four.hovered = false;
                            }
                        }
                        Button {
                            id: updateBtn;
                            text: "update";
                            onClicked: {
                                bezierCurve.createBezierCurve(myList[0],myList[1],myList[2],myList[3],myList[4],myList[5]);
                                bezierCurve.update(agvMap.series(0));
                            }
                        }
                        Button {
                            id: pointBtn;
                            text: "point";
                            onClicked: {
                                painCurver();
                            }
                        }
                    }
                }
            }
        }
    }
    property var choosePointId: 0;
    // 所有的按钮都不选择，点击坐标位置时需要重新点击按钮
    function allNoChoose()
    {
        start.hovered = false;
        stop.hovered = false;
        one.hovered = false;
        two.hovered = false;
        three.hovered = false;
        four.hovered = false;
        choosePointId = 0;
    }
    // 给当前所在的按钮赋id值，以便于操作点获取位置信息
    function buttonFun(id)
    {
        allNoChoose();
        id.hovered = true;
        choosePointId = id;
    }
    // 获取鼠标点击的坐标  更新存入数据和显示 以及更新地图
    function choosePoint()
    {
        if (choosePointId.hovered) {
            // 存入数据
            hoveredPoint = limitMaxMin(hoveredPoint.x, hoveredPoint.y);
            myList[pointIndex] = hoveredPoint;
            // 界面显示
            var text = hoveredPoint.x.toFixed(2) + " " + hoveredPoint.y.toFixed(2);
            choosePointId.text = text;
        }
        updatePoint();
    }
    function textTopPoint(text)
    {
        var arry = new Array();
        var point = Qt.point(0,0);
        arry = text.split(" ")
        point = limitMaxMin(arry[0], arry[1]);
        return point;
    }
    function limitMaxMin(x, y)
    {
        var point = Qt.point(0,0);
        if (x < minAxisX) {
            point.x = minAxisX;
        } else if (x > maxAxisX) {
            point.x = maxAxisX;
        } else {
            point.x = x;
        }
        if (y < minAxisY) {
            point.y = minAxisY;
        } else if (y > maxAxisY) {
            point.y = maxAxisY;
        } else {
            point.y = y;
        }
        return point;
    }



    function caculatePointToText(p)
    {
        var text1 = p[0].x.toFixed(2) + " " + p[0].y.toFixed(2);
        var text2 = p[1].x.toFixed(2) + " " + p[1].y.toFixed(2);
        var text3 = p[2].x.toFixed(2) + " " + p[2].y.toFixed(2);
        var text4 = p[3].x.toFixed(2) + " " + p[3].y.toFixed(2);
        one.text = text1;
        two.text = text2;
        three.text = text3;
        four.text = text4;
    }
    function painCurver()
    {
        var p;
        var i;
        p = bezierCurve.calcPoint(myList[0], Number(startRot.text) / 360 * 2 * 3.1416, myList[1], Number(stopRot.text) / 360 * 2 * 3.1416);
        for (i = 2; i < 6; i++) {
            myList[i] = p[i -2];
        }
        caculatePointToText(p);
        updateFourPoint();
    }
    // 更新点坐标
    function updatePoint()
    {
        switch (pointIndex)
        {
            case 0:
                startPointScat.visible = true;
                if (myList[0] != undefined) {
                    startPointScat.clear();
                    startPointScat.append(myList[0].x, myList[0].y);
                }
                break;
            case 1:
                stopPointScat.visible = true;
                if (myList[1] != undefined) {
                    stopPointScat.clear();
                    stopPointScat.append(myList[1].x, myList[1].y);
                }
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                updateFourPoint();
                break;
            default:
                break;
        }
    }
    function updateFourPoint()
    {
        pointScat.visible = true;
        pointScat.clear();
        pointScat.visible = true;
        if (myList[2] != undefined) {
            pointScat.append(myList[2].x, myList[2].y);
        }
        if (myList[3] != undefined) {
            pointScat.append(myList[3].x, myList[3].y);
        }
        if (myList[4] != undefined) {
            pointScat.append(myList[4].x, myList[4].y);
        }
        if (myList[5] != undefined) {
            pointScat.append(myList[5].x, myList[5].y);
        }
    }
    Component {
        id: textFieldComp2;
        TextFieldStyle {
            textColor: "black";
            background: Rectangle {
                id: bkgnd2;
                radius: 4;
                implicitWidth: 50;
                implicitHeight: 30;
                border.color: "#222";
                border.width: 1;
            }
        }
    }
}
